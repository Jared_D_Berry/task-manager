const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'jaredb8@gmail.com',
        subject: 'Welcome to the Task App',
        text: `welcome to the app, ${name}. Thank you for joining in! Please let me know how you get along with the app.`

    })
}

const sendCancelationEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'jaredb8@gmail.com',
        subject: 'Sorry to se you go!',
        text: `GoodBye, ${name},  We are so sorry to see you leave the Task App. How can we improve this App, was there anything we can do to keep you on board? I hope jto see you back sometime soon.`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendCancelationEmail
}